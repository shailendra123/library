import * as i0 from "@angular/core";
import * as i1 from "./public.component";
import * as i2 from "./address.pipe";
export declare class PublicModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<PublicModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<PublicModule, [typeof i1.PublicComponent, typeof i2.AddressPipe], never, [typeof i1.PublicComponent, typeof i2.AddressPipe]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<PublicModule>;
}
