import { NgModule } from '@angular/core';
import { PublicComponent } from './public.component';
import { AddressPipe } from './address.pipe';
import * as i0 from "@angular/core";
export class PublicModule {
}
PublicModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
PublicModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicModule, declarations: [PublicComponent,
        AddressPipe], exports: [PublicComponent, AddressPipe] });
PublicModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicModule, imports: [[]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        PublicComponent,
                        AddressPipe
                    ],
                    imports: [],
                    exports: [
                        PublicComponent, AddressPipe
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL3B1YmxpYy9zcmMvbGliL3B1YmxpYy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOztBQWU3QyxNQUFNLE9BQU8sWUFBWTs7eUdBQVosWUFBWTswR0FBWixZQUFZLGlCQVRyQixlQUFlO1FBQ2YsV0FBVyxhQUtYLGVBQWUsRUFBRSxXQUFXOzBHQUduQixZQUFZLFlBTmQsRUFDUjsyRkFLVSxZQUFZO2tCQVh4QixRQUFRO21CQUFDO29CQUNSLFlBQVksRUFBRTt3QkFDWixlQUFlO3dCQUNmLFdBQVc7cUJBQ1o7b0JBQ0QsT0FBTyxFQUFFLEVBQ1I7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGVBQWUsRUFBRSxXQUFXO3FCQUM3QjtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQdWJsaWNDb21wb25lbnQgfSBmcm9tICcuL3B1YmxpYy5jb21wb25lbnQnO1xuaW1wb3J0IHsgQWRkcmVzc1BpcGUgfSBmcm9tICcuL2FkZHJlc3MucGlwZSc7XG5cblxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBQdWJsaWNDb21wb25lbnQsXG4gICAgQWRkcmVzc1BpcGVcbiAgXSxcbiAgaW1wb3J0czogW1xuICBdLFxuICBleHBvcnRzOiBbXG4gICAgUHVibGljQ29tcG9uZW50LCBBZGRyZXNzUGlwZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIFB1YmxpY01vZHVsZSB7IH1cbiJdfQ==