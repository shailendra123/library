import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
export class AddressPipe {
    constructor() {
        this.show = '';
    }
    transform(value) {
        // return value+'hii value';
        for (const [key, value1] of Object.entries(value)) {
            this.show += value1 + " ,";
            // console.log(`${key}: ${value1}`);
        }
        return this.show;
        // return value;
    }
}
AddressPipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: AddressPipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
AddressPipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: AddressPipe, name: "address" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: AddressPipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'address'
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkcmVzcy5waXBlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vcHJvamVjdHMvcHVibGljL3NyYy9saWIvYWRkcmVzcy5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDOztBQUtwRCxNQUFNLE9BQU8sV0FBVztJQUh4QjtRQUlBLFNBQUksR0FBQyxFQUFFLENBQUM7S0FXUDtJQVZDLFNBQVMsQ0FBQyxLQUFRO1FBQ2hCLDRCQUE0QjtRQUM1QixLQUFLLE1BQU0sQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNqRCxJQUFJLENBQUMsSUFBSSxJQUFFLE1BQU0sR0FBQyxJQUFJLENBQUM7WUFDdkIsb0NBQW9DO1NBRXZDO1FBQ0gsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ2IsZ0JBQWdCO0lBQ2xCLENBQUM7O3dHQVhVLFdBQVc7c0dBQVgsV0FBVzsyRkFBWCxXQUFXO2tCQUh2QixJQUFJO21CQUFDO29CQUNKLElBQUksRUFBRSxTQUFTO2lCQUNoQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnYWRkcmVzcydcbn0pXG5leHBvcnQgY2xhc3MgQWRkcmVzc1BpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcbnNob3c9Jyc7XG4gIHRyYW5zZm9ybSh2YWx1ZTp7fSApOiBhbnkge1xuICAgIC8vIHJldHVybiB2YWx1ZSsnaGlpIHZhbHVlJztcbiAgICBmb3IgKGNvbnN0IFtrZXksIHZhbHVlMV0gb2YgT2JqZWN0LmVudHJpZXModmFsdWUpKSB7XG4gICAgICB0aGlzLnNob3crPXZhbHVlMStcIiAsXCI7XG4gICAgICAvLyBjb25zb2xlLmxvZyhgJHtrZXl9OiAke3ZhbHVlMX1gKTtcbiAgICAgIFxuICB9XG5yZXR1cm4gdGhpcy5zaG93O1xuICAgIC8vIHJldHVybiB2YWx1ZTtcbiAgfVxufVxuIl19